### Init
Install `npm` dependencies in the functions directory locally, by running: `cd functions; npm install;`
```
firebase functions:config:set sendgrid.api_key="your_api_key"
firebase functions:config:set email.from_email="from_address@email.com"
firebase functions:config:set email.destination_email="destination_address@email.com"
firebase functions:config:get > .runtimeconfig.json
```

### Run function
```
npm run start
onCreateOrder(require('./templates/test'))
```
