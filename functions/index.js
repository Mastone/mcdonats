const admin = require('firebase-admin')
const functions = require('firebase-functions')
const sendgridMail = require('@sendgrid/mail')
const emailTemplate = require('./templates/create-order')
const _sumBy = require('lodash/sumBy')
admin.initializeApp(functions.config().firebase)
sendgridMail.setApiKey(functions.config().sendgrid['api_key'])

const firestore = admin.firestore()
firestore.settings({ timestampsInSnapshots: true })

const getProducts = async (ids) => {
  const refs = ids.map(id => firestore.doc(`products/${id}`))
  const products = await firestore.getAll(...refs)
  return products.map(doc => { return { id: doc.id, ...doc.data() } })
}

const sendEmail = async (data) => {
  const emailDetails = {
    to: functions.config().email['destination_email'],
    from: functions.config().email['from_email'],
    subject: 'Заказ с сайта Макдонатс',
    html: emailTemplate(data)
  }

  try {
    await sendgridMail.send(emailDetails)
  } catch (error) {
    console.error(error.toString())
  }
}

exports.onCreateOrder = functions.firestore.document('orders/{id}').onCreate(
  async (snap, context) => {
    const data = snap.data()
    const cartLinesKeys = Object.keys(data.cartLines)
    const totalAmount = _sumBy(cartLinesKeys, key => data.cartLines[key])

    const products = await getProducts(cartLinesKeys)
    const totalPrice = _sumBy(products, (product) => {
      return product.price * data.cartLines[product.id]
    })
    const result = {
      ...data,
      products,
      totalAmount,
      totalPrice
    }

    await sendEmail(result)
    return null
  })
