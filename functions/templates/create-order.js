const _template = require('lodash/template')

const emailTemplate = _template(`
  <html>
    <head>
      <style>
        .font-weight_bold {
          font-weight: bold;
        }
        .contacts_right {
          padding-left: 30px;
        }
        .table {
          border-collapse: collapse;
          margin-top: 25px;
        }
        .table, .table th, .table td {
          border: 1px solid black;
        }
        .table th, .table td {
          text-align: center;
          padding: 5px;
        }
        .confirm {
          margin-top: 20px;
        }
      </style>
    </head>
    <body>
      <table>
        <tbody>
          <tr>
            <td>Адрес:</td>
            <td class="contacts_right">
              <%= address.street %>, д.<%= address.house %>
              <%= address.flat && ', кв. ' + address.flat + '<br>' %>
              <%= address.porch && 'Подъезд: ' + address.porch + '<br>' %>
              <%= address.doorСode && 'Код двери: ' + address.doorСode + '<br>' %>
              <%= address.floor && 'Этаж: ' + address.floor %>
            </td>
          </tr>
          <tr>
            <td>Получатель:</td>
            <td class="contacts_right"><%= user.name %></td>
          </tr>
          <tr>
            <td>Контакт получателя:</td>
            <td class="contacts_right"><%= user.phone %></td>
          </tr>
          <tr class="font-weight_bold">
            <td>Примечание:</td>
            <td class="contacts_right">
              <%= description && description + '<br>' %>
              Имя получателя: <%= user.name %> <br>
              Телефон получателя: <%= user.phone %> <br>
              <%= user.email && 'Email получателя: ' + user.email %>
            </td>
          </tr>
        </tbody>
      </table>

      <table class="table">
        <thead>
          <tr>
            <th>№</th>
            <th>Название</th>
            <th>Кол-во, шт.</th>
            <th>Цена/ед.</th>
            <th>Цена/общая</th>
          </tr>
        </thead>
        <tbody>
          <% products.forEach(function(product, idx) { %>
            <tr>
              <td><%= idx + 1 %></td>
              <td><%= product.name %></td>
              <td><%= cartLines[product.id] %></td>
              <td><%= product.price %></td>
              <td><%= product.price * cartLines[product.id] %></td>
            </tr>
          <% }); %>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2">Итого:</td>
            <td><%= totalAmount %></td>
            <td colspan="2" class="font-weight_bold"><%= totalPrice %></td>
          </tr>
        </tfoot>
      </table>

      <div class="confirm">
        <b>Товар получен, проверен, претензий нет</b> _____________ (подпись)
      </div>
      <div>Уважаемый клиент, просьба проверить товар и пересчитать его.</div>
    </body>
  </html>
`)

module.exports = emailTemplate
