import Vue from 'vue'
import VueScrollactive from 'vue-scrollactive'
import { Element, locale } from './element'
import VueTheMask from 'vue-the-mask'

Vue.use(VueTheMask)
Vue.use(VueScrollactive)
Vue.use(Element, { locale })
