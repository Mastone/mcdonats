import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import './element-variables.scss'

export { Element, locale }
