import Vue from 'vue'
import currency from './currency'
import pluralize from './pluralize'

Vue.filter('currency', currency)
Vue.filter('pluralize', pluralize)
