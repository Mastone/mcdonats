const currency = (number) => {
  if (!number) return '0 ₽'

  const str = number.toFixed(2).replace('.00', '')
  return str.length > 3
    ? `${str.slice(0, str.length - 3)} ${str.slice(str.length - 3)} ₽`
    : `${str} ₽`
}

export default currency
