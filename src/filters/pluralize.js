const pluralize = (n, titles) => {
  if (n < 0 || !titles || titles.length !== 3) return ''

  // eslint-disable-next-line standard/computed-property-even-spacing
  return titles[(n % 10 === 1 && n % 100 !== 11)
    ? 0
    : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2]
}

export default pluralize
