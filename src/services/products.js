import Vue from 'vue'

const service = new Vue({
  data () {
    return {
      loading: true
    }
  },
  methods: {
    setLoading (val) {
      this.loading = val
    }
  }
})

export default service
