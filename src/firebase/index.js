import * as firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyC8pmO1PdP272Um6xJzPk0tbotMPhoNVeI',
  authDomain: 'mcdonats.firebaseapp.com',
  databaseURL: 'https://mcdonats.firebaseio.com',
  projectId: 'mcdonats',
  storageBucket: 'mcdonats.appspot.com',
  messagingSenderId: '263523752257'
}

firebase.initializeApp(config)
const db = firebase.firestore()

export default db
