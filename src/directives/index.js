import Vue from 'vue'
import Sticky from './sticky'

Vue.directive('sticky', Sticky)
