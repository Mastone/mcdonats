import throttle from 'lodash/throttle'

const defaults = {
  throttleWait: 100,
  class: 'navigation_sticky'
}

const vSticky = {
  inserted (el, binding) {
    const params = { ...defaults, ...binding.value }
    const top = el.offsetTop
    let sticky = false

    const onScroll = throttle(() => {
      const needSticky = window.pageYOffset > top

      if (sticky !== needSticky) {
        sticky = needSticky
        el.classList.toggle(params.class)
      }
    }, params.throttleWait)

    window.addEventListener('scroll', onScroll)
    el.$destroy = () => window.removeEventListener('scroll', onScroll)
  },
  unbind (el) {
    el.$destroy()
  }
}

export default vSticky
