import sumBy from 'lodash/sumBy'

export default {
  inCartProductsTotalAmount (state, getters) { // Compute in cartLines?
    return sumBy(getters.inCartProducts, (product) => {
      return state.cartLines[product.id]
    })
  },
  inCartProductsTotalPrice (state, getters) {
    return sumBy(getters.inCartProducts, (product) => {
      return product.price * state.cartLines[product.id]
    })
  },
  inCartProducts (state) {
    const products = []

    Object.keys(state.cartLines).forEach((key) => {
      const product = state.products.find((product) => {
        return product.id === key
      })
      product && products.push(product)
    })
    return products
  }
}
