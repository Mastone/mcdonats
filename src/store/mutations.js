import Vue from 'vue'
import LocalStorage from '@/local-storage'

export default {
  initProducts (state, products) {
    state.products = products
  },
  initMenu (state, menu) {
    state.menu = menu
  },
  initCartLines (state, cartLines) {
    state.cartLines = cartLines
  },
  clearCartLines (state, cartLines) {
    state.cartLines = {}
    LocalStorage.sync('cartLines', state.cartLines)
  },
  addProductInCart (state, product) {
    Vue.set(state.cartLines, product.id, 1)
    LocalStorage.sync('cartLines', state.cartLines)
  },
  updateProductInCart (state) {
    LocalStorage.sync('cartLines', state.cartLines)
  },
  removeProductInCart (state, product) {
    Vue.delete(state.cartLines, product.id)
    LocalStorage.sync('cartLines', state.cartLines)
  }
}
