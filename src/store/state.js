export default {
  cartLines: {}, // init in localStorage
  products: [], // init in firebase
  menu: {
    donuts: [],
    drinks: [],
    iceCream: []
  }
}
