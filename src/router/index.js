import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'

Vue.use(Router)

const getActiveLink = (hash) => {
  const links = document.querySelectorAll('.scrollactive-item.navigation__link')
  return [...links].find((link) => {
    return link.getAttribute('href') === hash
  })
}

const triggerScrollActive = (hash) => {
  const link = getActiveLink(hash)
  link && link.click()
  return false
}

const scrollBehavior = (to, from, savedPosition) => {
  if (savedPosition) {
    return savedPosition
  } else {
    if (to.hash) {
      if (document.querySelector(to.hash)) {
        const position = { selector: to.hash }
        return to.meta.manualScroll ? triggerScrollActive(to.hash) : position
      }
      return false
    }
    return { x: 0, y: 0 }
  }
}

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior
})

router.beforeEach((to, from, next) => {
  const t = 'Макдонатс Екатеринбург | Доставка пончиков. Звонок по телефону 8 800 777-77-77'
  const title = to.name === 'home' ? t : `${to.meta.title} | ${t}`
  document.title = `🍩 ${title}`
  next()
})

export default router
