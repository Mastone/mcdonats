const routes = [
  {
    path: '/',
    name: 'home',
    meta: { title: 'Главная', manualScroll: true },
    component: () => import(/* webpackChunkName: "home" */ '@/views/Home.vue')
  },
  {
    path: '/contact-us',
    name: 'contact-us',
    meta: { title: 'Контакты', isNavMenuItem: true },
    component: () => import(/* webpackChunkName: "contact-us" */ '@/views/ContactUs.vue')
  },
  {
    path: '/about',
    name: 'about',
    meta: { title: 'О нас', isNavMenuItem: true },
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue')
  },
  {
    path: '/cart',
    name: 'cart',
    meta: { title: 'Корзина' },
    component: () => import(/* webpackChunkName: "cart" */ '@/views/Cart.vue')
  }
]

export default routes
