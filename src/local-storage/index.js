class LocalStorage {
  sync (key, payload) {
    if (key && payload) {
      localStorage.setItem(key, JSON.stringify(payload))
    } else {
      throw new Error('sync - expected key, payload')
    }
  }
}

export default new LocalStorage()
